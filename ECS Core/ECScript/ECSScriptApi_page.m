//
//  ECSScriptApi_page.m
//  ECS DevelopKit
//
//  Created by LittoCats on 8/21/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSScriptApi_page.h"
#import "NSObject+ECS.h"

@implementation ECSScriptApi_page

+ (void)analyseAction:(NSMutableDictionary *)action
{
    NSString *page_id = [action objectForKey:@"page"];
    if (!page_id || [page_id isEmpty])
        page_id = [[action objectForKey:ECSScriptContextKey] evaluateScript:[[NSString alloc] initWithFormat:@"if (typeof %@ !== 'undefined')  %@; else console.log('this context is not a page context .');",ECSViewControllerIdkeyInContext,ECSViewControllerIdkeyInContext]];
    if (!page_id || [page_id isEmpty]) return;
    ECSViewController *vc = [ECSViewController viewControllerWithIdentifier:page_id];
    NSAssert(vc, @"[ ECSScriptApi_page ] error , ECSViewController with identifier < %@ > not exist .",page_id);
    
    [action setObject:vc forKey:@"page"];
}

#pragma mark- page creation
+ (void)ECS_Script_API(open)
{
    NSString *identifier = arguments[0];
    NSDictionary *parameters = arguments.count > 1 ? arguments[1] : nil;
    NSDictionary *attributes = arguments.count > 2 ? arguments[2] : nil;
    
    //如果 identifier 指定的viewController 在当前 vc.navigationcontroller 的栈中,则直接显示
    ECSViewController *vc = [ECSViewController viewControllerWithIdentifier:identifier];
    if (!vc){
        //解析 viewcontroller path
        NSString *path = attributes[@"path"];
        if (attributes[@"path"]){
            
            if ([path hasPrefix:@"~/"])
                path = [path stringByResolvingSymlinksInPath];
            else if ([path hasPrefix:@"."])
                path = [[[(ECSViewController *)action[@"page"] attribute][@"path"] stringByAppendingPathComponent:path] stringByResolvingSymlinksInPath];
            
            attributes = [NSMutableDictionary dictionaryWithDictionary:attributes];
            [(NSMutableDictionary *)attributes setObject:path forKey:@"path"];
        }
        //如果 attributes 中不指定path ， 则使用使用根路径
        vc = [[ECSViewController alloc] initWithIdentifier:identifier parameter:parameters attribute:attributes];
    }
    
    [ECSViewController pushViewController:vc fromViewController:action[@"page"] animated:YES];
}

#pragma mark- page life ciecle
+ (void)ECS_Script_API(didLoad)
{
    [action[@"page"] setParameter:arguments[0] forKey:ECSViewControllerLiftCircle_viewDidLoad];
}
+ (void)ECS_Script_API(willAppear)
{
    [action[@"page"] setParameter:arguments[0] forKey:ECSViewControllerLiftCircle_viewWillAppear];
}
+ (void)ECS_Script_API(didAppear)
{
    [action[@"page"] setParameter:arguments[0] forKey:ECSViewControllerLiftCircle_viewDidAppear];
}
+ (void)ECS_Script_API(willDisappear)
{
    [action[@"page"] setParameter:arguments[0] forKey:ECSViewControllerLiftCircle_viewWillDisappear];
}
+ (void)ECS_Script_API(didDisappear)
{
    [action[@"page"] setParameter:arguments[0] forKey:ECSViewControllerLiftCircle_viewDidDisappear];
}

#pragma mark- 
+ (void)ECS_Script_API(navigator)
{
    [action[@"page"] updateNavigation:arguments[0] animated:NO];
}

#pragma mark-
+ (void)ECS_Script_API(hideNavigationBar)
{
    [[action[@"page"] navigationController] setNavigationBarHidden:[arguments[0] boolValue] animated:(arguments.count > 1 ? [arguments[1] boolValue] : YES )];
}
@end
