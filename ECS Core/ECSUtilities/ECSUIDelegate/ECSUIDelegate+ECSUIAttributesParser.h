//
//  ECSUIDelegate+ECSUIAttributesParser.h
//  ECS DevelopKit
//
//  Created by LittoCats on 9/5/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSUIDelegate.h"

@interface ECSUIDelegate (ECSUIAttributesParser)

- (void)view:(UIView *)view didUpdateAttribute:(NSString *)attribute;
@end
