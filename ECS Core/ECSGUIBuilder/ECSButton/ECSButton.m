//
//  ECSButton.m
//  ECS DevelopKit
//
//  Created by LittoCats on 9/7/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSButton.h"
#import "ECSUIBuilder.h"

#import "UIKit+ECS.h"
#import "NSObject+ECS.h"

#define kSpaceBetweenImageAndTitle 5.0

@interface ECSButton ()<ECSAutoLayoutProtocol>

@property(nonatomic,strong) UILabel     *titleLabel;
@property(nonatomic,strong) UIImageView *imageView;
@property (nonatomic, strong) UIImageView *backgroundView;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *highlightedTitle;

@property (nonatomic, strong) UIColor *titleColor;
@property (nonatomic, strong) UIColor *highlightedTitleColor;

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImage *highlightedImage;

@property (nonatomic, strong) UIImage *backgroundImage;
@property (nonatomic, strong) UIImage *highlightedBackgroundImage;

@property (nonatomic, strong) UIColor *highlightedBackgroundColor;

@property (nonatomic) UIControlState controlState; //normal or highlighted(pressed)

@end

@implementation ECSButton

+ (instancetype)buttonWithStyle:(ECSButtonStyle)style
{
    ECSButton *button = [[ECSButton alloc] init];
    button.style = style;
    return button;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self setTitle:self.ecsAttributes[@"title"] forState:UIControlStateNormal];
        UIImage *image = [UIImage imageWithScript:self.ecsAttributes[@"image"] context:self];
        [self setImage:image forState:UIControlStateNormal];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundView = [[UIImageView alloc] init];
        self.backgroundView.backgroundColor = [UIColor clearColor];
        [self addSubview:_backgroundView];
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:_titleLabel];
        
        self.imageView = [[UIImageView alloc] init];
        self.imageView.backgroundColor = [UIColor clearColor];
        [self addSubview:_imageView];
        
        self.adjustsImageWhenHighlighted = YES;
        self.adjustsImageWhenDisabled = YES;
        
        [super setBackgroundColor:[UIColor clearColor]];
        
        self.controlState = UIControlStateNormal;
        self.style = _style ? _style : ECSButtonStyleLeftImage;
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    self.controlState = UIControlStateHighlighted;
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    self.controlState = UIControlStateNormal;
}
- (void)setControlState:(UIControlState)state
{
    _controlState = state;
    self.titleLabel.text = [self titleForState:state];
    self.titleLabel.textColor = [self titleColorForState:state];
    self.imageView.image = [self imageForState:state];
    self.backgroundView.image = [self backgroundImageForState:state];
    self.backgroundView.backgroundColor = [self backgroundColorForState:state];
}
#pragma mark- 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.backgroundView setFrame:self.bounds];
    
    if (self.image) {
        CGFloat width = self.image.size.width, height = self.image.size.height;
        CGFloat sWidth = self.frame.size.width, sHieght = self.frame.size.height;
        
        CGFloat dWidth = sWidth;
        CGFloat dHeight = height * dWidth/width;
        if (dHeight > sHieght) {
            dHeight = sHieght;dWidth = width * dHeight/height;
        }
        self.imageView.frame =  (CGRect){0.0,0.0,dWidth,dHeight};
    }else
        self.imageView.frame = CGRectZero;
    
    CGSize size = [self.titleLabel sizeThatFits:CGSizeZero];
    
    if (_style == ECSButtonStyleLeftImage) {
        size.width = size.width+_imageView.frame.size.width > self.frame.size.width-kSpaceBetweenImageAndTitle ? self.frame.size.width-kSpaceBetweenImageAndTitle - _imageView.frame.size.width : size.width;
        self.titleLabel.frame = (CGRect){0.0,0.0,size.width,size.height};
        
        CGFloat dx = (self.frame.size.width - (size.width+_imageView.frame.size.width+kSpaceBetweenImageAndTitle))/2.0;
        _imageView.center = (CGPoint){_imageView.center.x+dx, self.frame.size.height/2.0};
        _titleLabel.center = (CGPoint){_imageView.center.x+ ((size.width+_imageView.frame.size.width))/2.0+kSpaceBetweenImageAndTitle, self.frame.size.height/2.0};
    }else if (_style == ECSButtonStyleRightImage){
        size.width = size.width+_imageView.frame.size.width > self.frame.size.width-kSpaceBetweenImageAndTitle ? self.frame.size.width-kSpaceBetweenImageAndTitle - _imageView.frame.size.width : size.width;
        self.titleLabel.frame = (CGRect){0.0,0.0,size.width,size.height};
        
        CGFloat dx = (self.frame.size.width - (size.width+_imageView.frame.size.width+kSpaceBetweenImageAndTitle))/2.0;
        _titleLabel.center = (CGPoint){_titleLabel.center.x+dx, self.frame.size.height/2.0};
        _imageView.center = (CGPoint){_titleLabel.center.x+ ((size.width+_imageView.frame.size.width))/2.0+kSpaceBetweenImageAndTitle, self.frame.size.height/2.0};
    }else if (_style == ECSButtonStyleTopImage){
        size.width = size.width > self.frame.size.width-kSpaceBetweenImageAndTitle ? self.frame.size.width-kSpaceBetweenImageAndTitle : size.width;
        self.titleLabel.frame = (CGRect){0.0,0.0,size.width,size.height};
        
        CGFloat dy = (self.frame.size.height - (size.height+_imageView.frame.size.width+kSpaceBetweenImageAndTitle))/2.0;
        
        _imageView.center = (CGPoint){self.frame.size.width/2.0, _imageView.center.y+dy};
        _titleLabel.center = (CGPoint){self.frame.size.width/2.0, _imageView.center.y+(_imageView.frame.size.height + _titleLabel.frame.size.height)/2.0 +kSpaceBetweenImageAndTitle};
    }else if (_style == ECSButtonStyleBottomImage){
        size.width = size.width > self.frame.size.width-kSpaceBetweenImageAndTitle ? self.frame.size.width-kSpaceBetweenImageAndTitle : size.width;
        self.titleLabel.frame = (CGRect){0.0,0.0,size.width,size.height};
        
        CGFloat dy = (self.frame.size.height - (size.height+_imageView.frame.size.width+kSpaceBetweenImageAndTitle))/2.0;
        
        _titleLabel.center = (CGPoint){self.frame.size.width/2.0, _titleLabel.center.y+dy};
        _imageView.center = (CGPoint){self.frame.size.width/2.0, _titleLabel.center.y+(_imageView.frame.size.height + _titleLabel.frame.size.height)/2.0 +kSpaceBetweenImageAndTitle};
    }
}

#pragma mark-
- (void)setTitle:(NSString *)title forState:(UIControlState)state
{
    state == UIControlStateNormal ? self.title = title : state == UIControlStateHighlighted ? self.highlightedTitle = title : nil;
    self.titleLabel.text = [self titleForState:state];
}
- (void)setTitleColor:(UIColor *)color forState:(UIControlState)state
{
    state == UIControlStateNormal ? self.titleColor = color : state == UIControlStateHighlighted ? self.highlightedTitleColor = color : nil;
    self.titleLabel.textColor = [self titleColorForState:state];
}
- (void)setImage:(UIImage *)image forState:(UIControlState)state
{
    state == UIControlStateNormal ? self.image = image : state == UIControlStateHighlighted ? self.highlightedImage = image : nil;
    self.imageView.image = [self imageForState:state];
}
- (void)setBackgroundImage:(UIImage *)image forState:(UIControlState)state
{
    state == UIControlStateNormal ? self.backgroundImage = image : state == UIControlStateHighlighted ? self.highlightedBackgroundImage = image : nil;
    self.backgroundView.image = [self backgroundImageForState:state];
}

- (void)setBackgroundColor:(UIColor *)color forState:(UIControlState)state
{
    state == UIControlStateNormal ? [super setBackgroundColor:color] : state == UIControlStateHighlighted ? self.highlightedBackgroundColor = color : nil;
    self.backgroundView.backgroundColor = [self backgroundColorForState:state];
}
- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    [self setBackgroundColor:backgroundColor forState:UIControlStateNormal];
}

- (NSString *)titleForState:(UIControlState)state
{
    return state == UIControlStateNormal || !_highlightedTitle ? self.title : state == UIControlStateHighlighted ? self.highlightedTitle : nil;
}
- (UIColor *)titleColorForState:(UIControlState)state
{
    return state == UIControlStateNormal ? self.titleColor : state == UIControlStateHighlighted ? !_highlightedTitleColor  ? [self.titleColor brightColor:-0.3] : self.highlightedTitleColor : nil;
}
- (UIImage *)imageForState:(UIControlState)state
{
    return state == UIControlStateNormal || !_highlightedImage ? self.image : state == UIControlStateHighlighted ? self.highlightedImage : nil;
}
- (UIImage *)backgroundImageForState:(UIControlState)state
{
    return state == UIControlStateNormal || _highlightedBackgroundImage ? self.backgroundImage : state == UIControlStateHighlighted ? self.highlightedBackgroundImage : nil;
}
- (UIColor *)backgroundColorForState:(UIControlState)state
{
    return state == UIControlStateNormal ? [super backgroundColor] : state == UIControlStateHighlighted ? !_highlightedBackgroundColor ? [[super backgroundColor] brightColor:-0.3] : self.highlightedBackgroundColor : nil;
}
- (UIColor *)backgroundColor
{
    return [self backgroundColorForState:UIControlStateNormal];
}

#pragma mark-
+ (NSDictionary *)autoLayoutAttributes
{
    return @{@"style":@[@"left",
                        ^NSNumber *(NSString *style){
                            return [NSNumber numberWithChar:[style UTF8String][0]];
                        }],
             @"titleColor":@[@"#3570F4",
                             ^UIColor *(NSString *script){
                                 return [UIColor colorWithScript:script];
                             }],
             @"backgroundColor":@[@"clear",
                                  ^UIColor *(NSString *script){
                                      return [UIColor colorWithScript:script];
                                  }],
             @"imageSrc":@[NSNull.null],
             };
}
@end
