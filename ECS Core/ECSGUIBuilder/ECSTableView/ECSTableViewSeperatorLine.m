//
//  ECSTableViewSeperatorLine.m
//  ECS DevelopKit
//
//  Created by LittoCats on 9/3/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSTableViewSeperatorLine.h"
#import "UIKit+ECS.h"

@interface ECSTableViewSeperatorLine ()

@property (nonatomic, strong) NSMapTable *constraintTable;

#define kECSConstraintLeft @"kECSConstraintLeft"
#define kECSConstraintWidth @"kECSConstraintRight"
#define kECSConstraintHeight @"kECSConstraintTop"
#define kECSConstraintBottom @"kECSConstraintBottom"

@end

@implementation ECSTableViewSeperatorLine

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.constraintTable = [NSMapTable strongToWeakObjectsMapTable];
        self.offset = UIEdgeInsetsMake(0, 10, 0, 0);
        self.height = 1.0;
        self.backgroundColor = [UIColor lightGrayColor];
        
        self.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return self;
}

- (void)setOffset:(UIEdgeInsets)offset
{
    _offset = UIEdgeInsetsMake(0.0, offset.left, 0.0, offset.right);
    
    [[self.constraintTable objectForKey:kECSConstraintLeft] setConstant:_offset.left];
    [[self.constraintTable objectForKey:kECSConstraintWidth] setConstant:_offset.right];
    if (self.superview) [self.superview setNeedsUpdateConstraints];
}

- (void)setHeight:(CGFloat)height
{
    _height = height;
    
    [[self.constraintTable objectForKey:kECSConstraintHeight] setConstant:- _height];
    if (self.superview) [self.superview setNeedsUpdateConstraints];
}

- (void)setBackground:(id)background
{
    _background = background;
    if (!background)
        self.backgroundColor = [UIColor clearColor];
    else if ([background isKindOfClass:[UIColor class]])
        self.backgroundColor = background;
    else if ([background isKindOfClass:[NSString class]])
        self.backgroundColor = [UIColor colorWithScript:background];
    else if ([background isKindOfClass:[UIImage class]])
        self.backgroundColor = [UIColor colorWithPatternImage:background];
    else
        self.backgroundColor = [UIColor lightGrayColor];
}

#pragma mark-
- (void)didMoveToSuperview
{
    NSLayoutConstraint *constraintLeft = [NSLayoutConstraint constraintWithItem:self
                                                       attribute:NSLayoutAttributeLeft
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:self.superview
                                                       attribute:NSLayoutAttributeLeft
                                                      multiplier:1
                                                        constant:self.offset.left];
    NSLayoutConstraint *constraintWidth = [NSLayoutConstraint constraintWithItem:self
                                                       attribute:NSLayoutAttributeWidth
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:self.superview
                                                       attribute:NSLayoutAttributeWidth
                                                      multiplier:1
                                                        constant:- self.offset.left-self.offset.right];
    NSLayoutConstraint *constraintBottom = [NSLayoutConstraint constraintWithItem:self
                                                       attribute:NSLayoutAttributeBottom
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:self.superview
                                                       attribute:NSLayoutAttributeBottom
                                                      multiplier:1
                                                        constant:0];
    NSLayoutConstraint *constraintHeight = [NSLayoutConstraint constraintWithItem:self
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1
                                                          constant:self.height];
    [self.constraintTable setObject:constraintHeight forKey:kECSConstraintHeight];
    [self.constraintTable setObject:constraintWidth forKey:kECSConstraintWidth];
    [self.constraintTable setObject:constraintLeft forKey:kECSConstraintLeft];
    [self.constraintTable setObject:constraintBottom forKey:kECSConstraintBottom];
    
    [self.superview addConstraint:constraintLeft];
    [self.superview addConstraint:constraintWidth];
    [self.superview addConstraint:constraintBottom];
    [self.superview addConstraint:constraintHeight];
}
@end
