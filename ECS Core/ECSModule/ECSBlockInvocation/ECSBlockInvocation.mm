//
//  ECSBlockInvocation.m
//  ECS DevelopKit
//
//  Created by LittoCats on 8/29/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSBlockInvocation.h"

struct BlockDescriptor {
    unsigned long reserved;
    unsigned long size;
    void *rest[1];
};

struct Block {
    void *isa;
    int flags;
    int reserved;
    void *invoke;
    struct BlockDescriptor *descriptor;
};

static const char *BlockSig(id blockObj)
{
    struct Block *block = (__bridge void *)blockObj;
    struct BlockDescriptor *descriptor = block->descriptor;
    
    int copyDisposeFlag = 1 << 25;
    int signatureFlag = 1 << 30;
    
    assert(block->flags & signatureFlag);
    
    int index = 0;
    if(block->flags & copyDisposeFlag)
        index += 2;
    
    return descriptor->rest[index];
}

@implementation ECSBlockInvocation

+ (id)invokeBlock:(id)block withArguments:(NSArray *)arguments
{
    return nil;
}


@end
