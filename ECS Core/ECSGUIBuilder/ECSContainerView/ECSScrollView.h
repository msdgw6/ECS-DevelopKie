//
//  ECSScrollView.h
//  ECS DevelopKit
//
//  Created by LittoCats on 9/1/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//
//  ScrollView 为容器 view,只能包含一个 subview, 且subview 永远靠左上排列
//
//
//


#import <UIKit/UIKit.h>

@interface ECSScrollView : UIScrollView

@end
