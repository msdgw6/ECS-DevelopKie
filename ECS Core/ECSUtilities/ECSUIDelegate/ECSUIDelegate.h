//
//  ECUIDelegate.h
//  ECDevelopKit
//
//  Created by LittoCats on 8/13/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit+ECS.h"
#import "ECSScriptContext.h"


#ifndef ECSUIDelegateEventKey
#define ECSUIDelegateEventKey
#endif


//控件的用户事件回调的 script 实现 key
//script 实现在控件的 ecsAttributes 中
#ifdef ECSUIDelegateEventKey

/**
 *  @ UIResponder 被单击时的 script 回调
 *      回调参数为 button.ecsAttributes
 */
#define ECSSingleClickedKey "__ECSButtonClickedKey"

/**
 *  @ barItem 被点击时的 script 回调
 *      回调参数为 item.ecsAttributes
 */
#define ECSItemClickedKey @"__ECSItemClickedKey"

/**
 *  @ tableView 等, cell被点击时的 script 回调
 *      回调第一个参数为 index.row, 第个参数为  index.section, 第三个参数为 cell.ecsAttributes
 */
#define ECSItemClickedAtIndexKey "__ECSItemClickedAtIndexKey"

/**
 *  @ actionSheet alertview 等中若有一组按钮(button),则按钮被点击时，触发该事件，
 *      事件回调第一个参数为 index
 */
#define ECSButtonClickedAtIndexKey @"__ECSButtonClickedAtIndexKey"

#endif

@interface ECSUIDelegate : NSObject

/**
 *  @instance
 *  程序运行中只有一个实列，通过 init 方法返回的也是唯一实例
 */
+ (instancetype)defaultInstance;


/**
 *  为 view  添加用户触摸事件响应
 */
+ (void)addOnClickEvent:(id)callback forResponder:(UIResponder *)responder;
@end
