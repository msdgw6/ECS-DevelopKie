//
//  ECScriptApi.h
//  ECDevelopKit
//
//  Created by LittoCats on 8/8/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ECSScriptContext.h"

#if TARGET_OS_IPHONE

/**
 *  @define api 减化写法，若有同步api，则返回类型必需为 id，且只能是 NSDictionary NSArray NSString NSNumber BOOL number(float int ...)
 */
#define ECS_Script_API(api) api:(NSArray *)arguments action:(NSMutableDictionary *)action completionHandler:(void (^)(id arg))handler

@interface ECSScriptApi : NSObject

/**
 *  定时器
    {time:NSDate/seconds,repeat:YES,callback:jsFunction}
    time 可以是小数，最小精度 0.001
    受
 */
+ (void)ECS_Script_API(timer);
@end

@protocol ECSScriptApiProtocol <NSObject>

@optional
/**
 *  @对 action 信息进行解析
 */
+ (void)analyseAction:(NSMutableDictionary *)action;

@end

#endif