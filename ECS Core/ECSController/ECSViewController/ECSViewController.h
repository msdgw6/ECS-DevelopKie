//
//  ECSViewController.h
//  ECS DevelopKit
//
//  Created by LittoCats on 8/17/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ECSProjectResourceRootPath [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"]

#define ECSViewControllerLiftCircle_viewDidLoad @"didLoad"
#define ECSViewControllerLiftCircle_viewWillAppear @"willAppear"
#define ECSViewControllerLiftCircle_viewDidAppear @"didAppear"
#define ECSViewControllerLiftCircle_viewWillDisappear @"willDisappear"
#define ECSViewControllerLiftCircle_viewDidDisappear @"didDisappear"

#define ECSViewControllerIdkeyInContext @"____ECSViewControllerIdkeyInContext"

@class ECSScriptContext;

@interface ECSViewController : UIViewController

/**
 *  @property viewController 唯一标识符，用于区分具有相同结构不同内容的 controller
    name>>custom_id 
        name viewcontroller 主标识，默认的初始化脚本名（脚本名不存在时，将默认使用 name 做为脚本名，
            name 不存在时，将抛出异常），可用字符 a-z,A-Z,0-9,_
        custom_id 自定义标识,可以省略,可用字符 a-z,A-Z,0-9,_
 */
@property (nonatomic, readonly) NSString *identifier;

/**
 *  @attribute readonly
        viewController 的配置信息，包含
        path 资源根路径 默认根路径为 {libraryPath}/{bundleidentifier}
        script 初始化脚本文件 默认 {主标识}.js,不包含扩展名
        layout view 布局文件 默认 {主标识}.json,不包含扩展名
        identifier  
        root BOOL 是否做为 rootViewController打开
 */
@property (nonatomic, readonly) NSDictionary *attribute;

/**
 *  @ECSScriptContext viewController 中 script 执行环境，与 controller 梆定
 *  @保留字 page_id (当前 controller 的 _id)
            current_path （当前 controller 工作路径，管理controller用到的资源）
 */
@property (nonatomic, readonly) ECSScriptContext *ecsContext;

/**
 *  @初始化方法
 */
- (id)initWithIdentifier:(NSString *)identifier parameter:(NSDictionary *)parameter attribute:(NSDictionary *)attribute;

/**
 *  @utility 根据 identifier 找到相应的 viewController（如果存在)
 */
+ (ECSViewController *)viewControllerWithIdentifier:(NSString *)identifier;
@end

@interface ECSViewController (ECSParameter)

/**
 *  @parameter viewcontroller 属性参数等缓存
 *
 *  @保留字 current_path （当前 controller 工作路径，管理controller用到的资源等）
 */
- (id)parameterForKey:(id<NSCopying>)key;

- (void)setParameter:(id)parameter forKey:(id<NSCopying>)key;
@end

@interface ECSViewController (ECSNavigationBar)

/**
 *  @更新导航栏
 {
    right:{}/[]
    left:{}
    title:{}
    back:{}
 }
 */
- (void)updateNavigation:(NSDictionary *)navigator animated:(BOOL)animated;

@end

@interface ECSViewController (ECSNavigationController)

/**
 *  @navigate 从某一 viewController 推出下一个 viewController
 *  @nvc 下一个要显示的 vc
 *  @ovc 
 *  @discussion 如果 ovc 不存在，则 nvc 以rootViewController 打开
 *      如果 ovc 不是当前navigationController的 top controller 
        则先弹出 ovc 上面的 vc ,然后弹出 nvc 
 */
+ (void)pushViewController:(ECSViewController *)nvc fromViewController:(ECSViewController *)ovc animated:(BOOL)animated;

/**
 *  @navigate 关闭 viewcontroller
 *  @vc 需要关闭的 viewcontroller
 *  @discussion 如果 vc 不是 navigationController 的top controller, 则 vc 上面的 controller 将全部被推出
 */
+ (void)popViewController:(ECSViewController *)vc animated:(BOOL)animated;
@end

@interface ECSViewController (ECSResourceManager)
- (NSData *)sourceDataInRelativePath:(NSString *)relativePath;
- (NSString *)sourceStringInRelativePath:(NSString *)relativePath type:(NSString *)type;
- (id)sourceInRelativePath:(NSString *)relativePath type:(NSString *)type;
@end