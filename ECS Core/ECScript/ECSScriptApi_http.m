//
//  ECSScriptApi_http.m
//  ECS DevelopKit
//
//  Created by LittoCats on 8/26/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSScriptApi_http.h"
#import "ECSNetWorking.h"

@implementation ECSScriptApi_http

+ (void)ECS_Script_API(download)
{
    int arg_length = arguments.count;
    NSAssert(arg_length, @"[ECSScriptApi_http] error : not enough parameters < download 至少需要一个参数 url >.");
    [ECSNetWorking downloadFrom:arguments[0]
                  withParameter:arg_length > 1 ? arguments[1] : nil
              completionHandler:^(NSString *dataPath, NSError *error) {
                  handler(dataPath);
              } progressHandler: arg_length > 2 ? ^(CGFloat rate) {
                  [ECSScriptContext callFunction:arguments[2] withArguments:@(rate), nil];
              } : nil];
}

@end
