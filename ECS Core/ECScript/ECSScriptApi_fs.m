//
//  ECSScriptApi_fs.m
//  ECS DevelopKit
//
//  Created by LittoCats on 8/26/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSScriptApi_fs.h"
#import "ECSArchiver.h"

#import "ECSUtilities.h"

#define kECSPathResolve(path) [path hasPrefix:@"/"] \
                                ? [path stringByResolvingSymlinksInPath] \
                                : [[NSHomeDirectory() stringByAppendingPathComponent:path] stringByResolvingSymlinksInPath]

@implementation ECSScriptApi_fs

+ (NSString *)ECS_Script_API(loadFile)
{
    if (![arguments count]) return nil;
    NSString *file = kECSPathResolve(arguments[0]);
    NSString *content = [[NSString alloc] initWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil];
    
    handler(content);
    return content;
}

+ (void)ECS_Script_API(unCompress)
{
    if (![arguments count]) return;
    NSString *srcFile = kECSPathResolve(arguments[0]);
    
    NSString *desPath = arguments.count > 1 ? arguments[1] : nil;
    if (!desPath || [desPath isEmpty])
        desPath = [srcFile stringByDeletingLastPathComponent];
    desPath = kECSPathResolve(desPath);
    [ECSArchiver uncompressFile:srcFile to:desPath completionHanlder:^(NSError *error) {
        handler(@(!error));
    }];
}
@end
