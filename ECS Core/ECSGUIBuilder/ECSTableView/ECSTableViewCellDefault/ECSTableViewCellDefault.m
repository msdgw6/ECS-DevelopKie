//
//  ECSTableViewCellDefault.m
//  ECS DevelopKit
//
//  Created by LittoCats on 9/4/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSTableViewCellDefault.h"

@implementation ECSTableViewCellDefault

- (NSArray *)contentViews
{
    _titlabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, 44)];
    _titlabel.text = @"ECSTableViewCellDefault";
    return @[_titlabel];
}

- (void)updateContentData
{
    _titlabel.text = self.contentData[@"title"];
}

+ (CGFloat)heightForContentData:(id)data
{
    return 44.0;
}

//optional
- (ECSTableViewSeperatorLine *)seperatorLine
{
    ECSTableViewSeperatorLine *line = [ECSTableViewSeperatorLine new];
    line.height = 0.5;
    line.background = @"#979797";
    line.offset = ECSTableViewSeperatorLineOffset(0, 0);
    return line;
}
@end
