//
//  UIKit+EC.h
//  ECDevelopKit
//
//  Created by LittoCats on 8/7/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ECSScriptContext;
//******************************************************************************************//

@interface UIApplication (ECS)

@property (nonatomic, readonly) ECSScriptContext *ecsContext;

/**
 *  @appCache
 *  @NSMapTable 用于缓存运行时信息，key 将被 retain，value 不会被 retain 
 */
@property (nonatomic, readonly) NSMapTable *ecsCache;

@end

//******************************************************************************************//


//******************************************************************************************//

@interface UIView (ECS)

/**
 *  @cache
 *  @NSMutableDictionar 用于缓存生命期内的非 @property 属性及临时变量
 */
@property (nonatomic, readonly) NSMutableDictionary *ecsAttributes;

@end

//******************************************************************************************//

typedef NSString ECSHexString;

@interface UIColor (ECS)

/**
 *  根据script生成 UIColor
 *  @params script 颜色描述
 *  example HEX(RGB/RGBA) #FFF #FFFF #FFFFFF #FFFFFFFF
 *          RGB/RGBA (r,g,b) (r,g,b,a)  r/g/b/a 范围为 0 － 255
 *          colorString red blue random ...
 *          image @imageName, image 必须为本地图片文件
 *
 */
+ (UIColor *)colorWithScript:(NSString *)script;

/**
 *  获取 UIColor 的描述
 *  return HEXString 格式 RGBA
 */
- (NSString *)script;

/**
 *  色值变亮
 *  @rate -1~1
 *  < 0 表示变暗
 */
- (UIColor *)brightColor:(CGFloat)rate;
@end

//******************************************************************************************//

@interface UIImage (ECS)

+ (instancetype)imageWithScript:(NSString *)script;

/**
 *  context 具有 ECSSCriptContext 属性的对像实例
 */
+ (instancetype)imageWithScript:(NSString *)script context:(id)context;

/**
 *  异步获取图片
 *  context 为图片的环境，当script中包含路径时，会使用 context 获取路径
 *  completionHandler 在主线程中执行
 */
+ (void)imageWithScript:(NSString *)script context:(id)context completionHandler:(void (^)(NSString *script, UIImage *image))completionHandler;

/**
 *  清空 图片 缓存
 */
+ (void)clearImageCache;
@end

//******************************************************************************************//

@interface UIFont (ECS)

/**
 *  根据描述，找到并返回 UIFont
 *  @param script: "fontName fontSize"
 */
+ (UIFont *)fontWithScript:(NSString *)script;

- (NSString *)script;
@end

//******************************************************************************************//

@interface UIImageView (ECS)

/**
 *  图片异步加载
 */
- (void)setImageWithScript:(NSString *)script;
- (void)setImageWithScript:(NSString *)script
               placeHolder:(UIImage *)placeHolder;
- (void)setImageWithScript:(NSString *)script
               placeHolder:(UIImage *)placeHolder
         completionHandler:(void (^)(NSString *script, UIImage *image))completionHandler;

@end