//
//  ECNetWorking.m
//  ECDevelopKit
//
//  Created by LittoCats on 8/7/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSNetWorking.h"
#import "AFNetworking.h"

@implementation ECSNetWorking

#pragma mark- ECScriptBridge send
+ (void)sendHttpRequest:(NSString *)urlString
                 method:(NSString *)method
             parameters:(NSDictionary *)parameters
                timeOut:(NSTimeInterval)timeOut
              cacheTime:(NSTimeInterval)cacheTime
      completionHandler:(void (^)(NSHTTPURLResponse *, id, NSError *))handler
{
    
}

+ (void)uploadTo:(NSString *)urlString
   withParameter:(NSDictionary *)parameter
completionHandler:(void (^)(NSURLResponse *, id, NSError *))completionHandler
 progressHandler:(void (^)(CGFloat))progressHandler
{
    
}

+ (void)downloadFrom:(NSString *)urlString
       withParameter:(NSDictionary *)parameter
   completionHandler:(void (^)(NSString *, NSError *))completionHandler
     progressHandler:(void (^)(CGFloat))progressHandler
{
    NSMutableURLRequest *request = [[[self manager] requestSerializer] requestWithMethod:@"GET" URLString:urlString parameters:parameter error:nil];
    NSString *tempPath = [NSString stringWithFormat:@"%@esc_download_temp_%p",NSTemporaryDirectory(),request];
    
    AFHTTPRequestOperation *download = [[self manager] HTTPRequestOperationWithRequest:request
                                                                               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                                   NSError *error;
                                                                                   NSString *downloadPath = [NSString stringWithFormat:@"%@%@",NSTemporaryDirectory(),[operation.response suggestedFilename]];
                                                                                   if ([[NSFileManager defaultManager] fileExistsAtPath:downloadPath])
                                                                                       [[NSFileManager defaultManager] removeItemAtPath:downloadPath error:&error];
                                                                                   
                                                                                   [[NSFileManager defaultManager] moveItemAtPath:tempPath toPath:downloadPath error:&error];
                                                                                   if (completionHandler)
                                                                                       completionHandler(downloadPath, error ? error : nil);
                                                                                   
                                                                               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                                   if (completionHandler)
                                                                                       completionHandler(nil, error);
                                                                               }];
    download.outputStream = [NSOutputStream outputStreamToFileAtPath:tempPath append:NO];
    
    [[self manager].operationQueue addOperation:download];
}

+ (AFHTTPRequestOperationManager *)manager
{
    static AFHTTPRequestOperationManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [AFHTTPRequestOperationManager manager];
        [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    });
    return manager;
}

/*************************************************************************************************/

+ (ECSSocket *)socketWithHost:(NSString *)host port:(int)port type:(NSString *)socket
{
    return nil;
}
@end

@implementation ECSSocket



@end