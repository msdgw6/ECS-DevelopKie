//
//  ECSScriptApi_page.h
//  ECS DevelopKit
//
//  Created by LittoCats on 8/21/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSScriptApi.h"
#import "ECSViewController.h"

@interface ECSScriptApi_page : ECSScriptApi

/**
 *  @open 打开一下新的页面
 *  @arguments[0] NSString page identifier 页面（viewController)标签
 *  @arguments[1] NSDictionary parameters 传递到新页面的参数
 *  @arguments[2] NSDictionary attributes 新页面属性，包含 script layout path root 等属性
 */
+ (void)ECS_Script_API(open);

/**
 *  
 */
+ (void)ECS_Script_API(didLoad);
+ (void)ECS_Script_API(willAppear);
+ (void)ECS_Script_API(didAppear);
+ (void)ECS_Script_API(willDisappear);
+ (void)ECS_Script_API(didDisappear);

/**
 *
 */
+ (void)ECS_Script_API(navigator);
@end
