//
//  ECSJavaScriptContext.h
//  ECS DevelopKit
//
//  Created by LittoCats on 8/27/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *
 */

@interface ECSJavaScriptContext : NSObject

/**
 *  @exceptionHandler block, 异常处理
 */
//@property (nonatomic, strong) void (^exceptionHandler)(id exception);

/**
 *  向context中添加属性
 *  @propertyName 属性名
 *  @property 属性值 当 property 为 block 时
 */
- (void)addProperty:(id)property withName:(NSString *)name;

/**
 *  运行 javascript
 */
- (id)evaluateJavaScript:(NSString *)script;

/**
 *  调用 js 环境中的function
 */
- (id)callFunction:(NSString *)function withArguments:(id)arg,... NS_REQUIRES_NIL_TERMINATION;
- (id)callFunction:(NSString *)function withArguments:(id)arg va_list:(va_list)vList;

@end