//
//  ECSUIDelegate+UIAlertView.m
//  ECS DevelopKit
//
//  Created by LittoCats on 8/19/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSUIDelegate+UIAlertView.h"

@implementation ECSUIDelegate (UIAlertView)

// Called when a button is clicked. The view will be automatically dismissed after this call returns
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

// Called when we cancel a view (eg. the user clicks the Home button). This is not called when the user clicks the cancel button.
// If not defined in the delegate, we simulate a click in the cancel button
//- (void)alertViewCancel:(UIAlertView *)alertView;

// before animation and showing view
//- (void)willPresentAlertView:(UIAlertView *)alertView;

// after animation
//- (void)didPresentAlertView:(UIAlertView *)alertView;

// before animation and hiding view
//- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex;

// after animation
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [(id<ECSScriptFunction>)alertView.ecsAttributes[ECSButtonClickedAtIndexKey] evaluateWithArguments:@(buttonIndex), [alertView buttonTitleAtIndex:buttonIndex], nil];
}

// Called after edits in any of the default fields added by the style
//- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView;

@end
