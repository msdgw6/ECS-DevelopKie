//
//  ECSScriptApi_app.m
//  ECS DevelopKit
//
//  Created by LittoCats on 8/26/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSScriptApi_app.h"
#import "ECSUIDelegate.h"

@implementation ECSScriptApi_app

+ (void)ECS_Script_API(alert)
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:arguments[0] delegate:[ECSUIDelegate defaultInstance] cancelButtonTitle:@"取消" otherButtonTitles: nil];
    for (int index = 1; index < [arguments count]; index ++){
        id argument = arguments[index];
        if ([argument isKindOfClass:[NSString class]])
            [alert addButtonWithTitle:argument];
        else
            [alert.ecsAttributes setObject:argument forKey:ECSButtonClickedAtIndexKey];
    }
    if (alert.numberOfButtons == 1)
        [alert addButtonWithTitle:@"确定"];
    
    [alert show];
}
@end
