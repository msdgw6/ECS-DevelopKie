//
//  ECNetWorking.h
//  ECDevelopKit
//
//  Created by LittoCats on 8/7/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ECSSocket;
@interface ECSNetWorking : NSObject

/**
 *  发送一个http异步请求
    @params 
    @urlString
    @method one of POST GET
    @parameters 参数列表，value 为 NSString/NSNumber/NSArray
    @timeOut    超时，单位为秒，当 timeOut 等于 0 时，timeOut 将被设置为 60
    @cacheTime  缓存时间，单位为秒，当 cacheTime 等于 0 时，表示不缓存
    @successHandler 请求成功后回调
 */
+ (void)sendHttpRequest:(NSString *)urlString
                 method:(NSString *)method
             parameters:(NSDictionary *)parameters
                timeOut:(NSTimeInterval)timeOut
              cacheTime:(NSTimeInterval)cacheTime
      completionHandler:(void (^)(NSHTTPURLResponse *response, id responsObject, NSError *error))handler;

/**
 *
 */
+ (void)downloadFrom:(NSString *)urlString
       withParameter:(NSDictionary *)parameter
   completionHandler:(void (^)(NSString *dataPath, NSError *error))completionHandler
     progressHandler:(void (^)(CGFloat rate))progressHandler;

/**
 *
 */
+ (void)uploadTo:(NSString *)urlString
       withParameter:(NSDictionary *)parameter
   completionHandler:(void (^)(NSURLResponse *response, id responseObject, NSError *error))completionHandler
     progressHandler:(void (^)(CGFloat rate))progressHandler;



/**
 *  @udpSocket tcpSocket
 */
+ (ECSSocket *)socketWithHost:(NSString *)host port:(int)port type:(NSString *)socket;
@end

@interface ECSSocket : NSObject

@end