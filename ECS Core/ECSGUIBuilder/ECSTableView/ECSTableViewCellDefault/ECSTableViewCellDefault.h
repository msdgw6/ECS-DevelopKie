//
//  ECSTableViewCellDefault.h
//  ECS DevelopKit
//
//  Created by LittoCats on 9/4/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSTableViewCell.h"

@interface ECSTableViewCellDefault : ECSTableViewCell<ECSTableSubview>

@property (nonatomic, strong) UILabel *titlabel;
@end
