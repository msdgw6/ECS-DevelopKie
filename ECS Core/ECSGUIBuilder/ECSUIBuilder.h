//
//  ECSUIBuilder.h
//  ECS DevelopKit
//
//  Created by LittoCats on 8/26/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ECSScriptContext;
@interface ECSUIBuilder : NSObject

/**
 *  @ 根据 json(NSDictionary) 文档生成view
 *  @type 必须指定，若不指定，将使用 UIView
 *  @discussion json 中的将会被拷贝到 新view的 ecsAttributes 中
 */
+ (UIView *)viewWithAttributes:(NSDictionary *)attributes context:(ECSScriptContext *)context;
@end

@interface ECSUIAttributesParser : NSObject
+ (id)parseAttributes:(id)attributes forView:(UIView *)view;

@end

typedef id(^ECSAutoLayoutAttributeParserBlock)(id);

#define ECS_UIViewAlignKey @"ECS_UIViewAlignKey"
#define ECS_UIViewMarginKey @"ECS_UIViewMarginKey"
#define ECS_UIViewPaddingKey @"ECS_UIViewPaddingKey"

typedef NS_OPTIONS(NSInteger, ECS_UIViewAlignMode) {
    ECS_UIViewAlignModeLeft         = 1 << 1,
    ECS_UIViewAlignModeRight        = 1 << 2,
    ECS_UIViewAlignModeTop          = 1 << 3,
    ECS_UIViewAlignModeBottom       = 1 << 4,
};

NSString *NSStringFromECS_UIViewAlignMode(ECS_UIViewAlignMode align);
ECS_UIViewAlignMode ECS_UIViewAlignModeFromString(NSString *align);

typedef UIEdgeInsets ECS_Margin;
typedef UIEdgeInsets ECS_Padding;

#define NSStringFromECS_Margin(margin) NSStringFromUIEdgeInsets(margin)
#define NSStringFromECS_Padding(padding) NSStringFromUIEdgeInsets(padding)

#define ECS_MarginFromString(margin) UIEdgeInsetsFromString(margin)
#define ECS_PaddingFromString(padding) UIEdgeInsetsFromString(padding)

@protocol ECSAutoLayoutProtocol <NSObject>

@optional
/**
 *  @autoLayoutAttributes
 *  返回 view 必须设置的属性，返回内容包含默认值
 *  UIView 已实现该协议
 *  自定义实现该协议时，只需要返回需要处理的内容,super 方法的实现将自动解析
 *  @return NSDictionary , key 为 attribute name，value 为默认的属性值、keyPath、valueParser(ECSAutoLayoutAttributeParserBlock)
 */
+ (NSDictionary *)autoLayoutAttributes;


#define ecs_AutoLayoutWrapContentWidthKey "ecs_AutoLayoutWrapContentWidthKey"
#define ecs_AutoLayoutWrapContentHeightKey "ecs_AutoLayoutWrapContentHeight"

/**
 *  @wrap_content   若要使view具有wrap_content (width/height)功能，需实现这两个方法
 */
- (void)ecs_AutoLayoutWrapContentWidth;
- (void)ecs_AutoLayoutWrapContentHeight;

@end

@protocol ECSAutoLayout <NSObject>

@required
/**
 *  @解析 subview 的约束
 *  当view 实现了该协议，在更新 padding 后，或者subview 更新 size/margin/align 后，会调用该方法
 */
- (void)parseContraintsForView:(UIView *)subView;
@end

@interface UIView (ECSUIBuilder)

@end