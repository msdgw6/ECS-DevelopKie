//
//  ECSUIDelegate+UIAlertView.h
//  ECS DevelopKit
//
//  Created by LittoCats on 8/19/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSUIDelegate.h"

@interface ECSUIDelegate (UIAlertView)<UIAlertViewDelegate>

@end
