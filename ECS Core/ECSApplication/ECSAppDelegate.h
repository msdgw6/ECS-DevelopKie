//
//  ECAppDelegate.h
//  ECDevelopKit
//
//  Created by LittoCats on 8/6/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ECSUtilities.h"

@interface ECSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
