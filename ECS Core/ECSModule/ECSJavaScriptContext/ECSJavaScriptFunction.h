//
//  ECSJavaScriptFunction.h
//  ECS DevelopKit
//
//  Created by LittoCats on 8/29/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ECSJavaScriptFunction <NSObject>

/**
 *  @ 执行 ECSJavaScriptFunction
 *  @ discussion ECSJavaScriptFunction 不保留执行环境信息及参数信息，因此执行时 可能因 context 不存在而失败
 */
- (id)applyWithArguments:(id)arg,... NS_REQUIRES_NIL_TERMINATION;

- (id)applyWithArguments:(id)arg va_list:(va_list)vList;

@end

@interface ECSJavaScriptFunction : NSObject <ECSJavaScriptFunction>

@end

