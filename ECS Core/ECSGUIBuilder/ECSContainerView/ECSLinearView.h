//
//  ECSLinearLayout.h
//  ECS DevelopKit
//
//  Created by LittoCats on 9/1/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//
//
//  linear view 中，子view 分横向排列（hor）和纵向排列(ver)。横向时，subview 从左右至右排列，纵向时,subview 从上至下排列
//  横向时 子 view 的 align 属性只有 top/bottom 起作用，纵向时， 只有 left/right 起作用
//  linear view 的 padding 属性，横向时, left/right 只有在 subview 的 width 为 fill_parent 或 百分比时起作用， 纵向时类比
//
//  linear view 横向时，height = wrap_content 无效，纵向时，width = wrap_content 无效
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(char, ECSLinearOrientation) {
    ECSLinearOrientationVertical        = 'v',
    ECSLinearOrientationHorizontal      = 'h'
};

@interface ECSLinearView : UIView

/**
 *  @orientation
 */
@property (nonatomic, assign) ECSLinearOrientation orientation;
@end
