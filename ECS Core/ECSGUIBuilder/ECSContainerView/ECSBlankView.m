//
//  ECSBlankView.m
//  ECS DevelopKit
//
//  Created by LittoCats on 9/1/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSBlankView.h"
#import "UIKit+ECS.h"
#import "ECSUIBuilder.h"
#import "ECSObjectAttributeWrapper.h"

@interface ECSBlankView () <ECSAutoLayout,ECSAutoLayoutProtocol>

@end

@implementation ECSBlankView

- (void)addSubview:(UIView *)view
{
    [super addSubview:view];
    [self parseContraintsForView:view];
}

- (void)willRemoveSubview:(UIView *)subview
{
    [super willRemoveSubview:subview];
    subview.translatesAutoresizingMaskIntoConstraints = YES;
}

#pragma mark- ECSAutoLayout
- (void)parseContraintsForView:(UIView *)subView
{
    subView.translatesAutoresizingMaskIntoConstraints = NO;
    
    ECS_Margin margin = ECS_MarginFromString(ECS_WrappedObjectAttribute(subView, [ECS_UIViewMarginKey UTF8String]));
    ECS_Padding padding = ECS_PaddingFromString(ECS_WrappedObjectAttribute(self, [ECS_UIViewPaddingKey UTF8String]));
    ECS_UIViewAlignMode align = ECS_UIViewAlignModeFromString(ECS_WrappedObjectAttribute(subView, [ECS_UIViewAlignKey UTF8String]));
    
    NSLayoutConstraint *constraintHor;
    NSLayoutConstraint *constraintVer;
    NSLayoutConstraint *constraintWidth;
    NSLayoutConstraint *constraintHeight;
    
    // horizontal
    NSLayoutAttribute attributeHor = (align & (ECS_UIViewAlignModeLeft | ECS_UIViewAlignModeRight)) == (ECS_UIViewAlignModeLeft | ECS_UIViewAlignModeRight) ? NSLayoutAttributeCenterX : align & ECS_UIViewAlignModeRight ? NSLayoutAttributeRight : NSLayoutAttributeLeft;
    CGFloat constantHor = attributeHor == NSLayoutAttributeLeft ? padding.left + margin.left : attributeHor == NSLayoutAttributeRight ? - padding.right - margin.right : 0;
    constraintHor = [NSLayoutConstraint constraintWithItem:subView
                                                 attribute:attributeHor
                                                 relatedBy:NSLayoutRelationEqual
                                                    toItem:self
                                                 attribute:attributeHor
                                                multiplier:1
                                                  constant:constantHor];
    // vertical
    NSLayoutAttribute attributeVer = (align & (ECS_UIViewAlignModeTop | ECS_UIViewAlignModeBottom)) == (ECS_UIViewAlignModeTop | ECS_UIViewAlignModeBottom) ? NSLayoutAttributeCenterY : align & ECS_UIViewAlignModeBottom ? NSLayoutAttributeBottom : NSLayoutAttributeTop;
    CGFloat constantVer = attributeVer == NSLayoutAttributeTop ? padding.top + margin.top : attributeVer == NSLayoutAttributeBottom ? - padding.bottom - margin.bottom : 0;
    constraintVer = [NSLayoutConstraint constraintWithItem:subView
                                                 attribute:attributeVer
                                                 relatedBy:NSLayoutRelationEqual
                                                    toItem:self
                                                 attribute:attributeVer
                                                multiplier:1
                                                  constant:constantVer];
    
    // width
    id width = subView.ecsAttributes[@"size"][@"width"];
    width = [width isEqual:@"fill_parent"] ? @"100%" : width;
    if ([width isKindOfClass:[NSString class]] && [width hasSuffix:@"%"]) {
        CGFloat multiplier = [width integerValue]/100.0;
        constraintWidth = [NSLayoutConstraint constraintWithItem:subView
                                                       attribute:NSLayoutAttributeWidth
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:self
                                                       attribute:NSLayoutAttributeWidth
                                                      multiplier:multiplier
                                                        constant:-multiplier*(padding.left+padding.right)-margin.left-margin.right];
    }else{
        if ([width isEqual:@"wrap_content"] && [subView respondsToSelector:@selector(ecs_AutoLayoutWrapContentWidth)]) {
            [(id<ECSAutoLayoutProtocol>)subView ecs_AutoLayoutWrapContentWidth];
        }else{
            CGFloat widthValue = [width floatValue];
            constraintWidth = [NSLayoutConstraint constraintWithItem:subView
                                                           attribute:NSLayoutAttributeWidth
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:self
                                                           attribute:NSLayoutAttributeWidth
                                                          multiplier:0
                                                            constant:widthValue ? widthValue : subView.frame.size.width];
        }
    }
    
    //height
    id height = subView.ecsAttributes[@"size"][@"height"];
    height = [height isEqual:@"fill_parent"] ? @"100%" : height;
    if ([height isKindOfClass:[NSString class]] && [height hasSuffix:@"%"]) {
        CGFloat multiplier = [height integerValue]/100.0;
        constraintHeight = [NSLayoutConstraint constraintWithItem:subView
                                                        attribute:NSLayoutAttributeHeight
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeHeight
                                                       multiplier:multiplier
                                                         constant:-multiplier*(padding.top+padding.bottom)-margin.top-margin.bottom];
    }else{
        if ([height isEqual:@"wrap_content"] && [subView respondsToSelector:@selector(ecs_AutoLayoutWrapContentHeight)]) {
            [(id<ECSAutoLayoutProtocol>)subView ecs_AutoLayoutWrapContentHeight];
        }else{
            CGFloat heightValue = [height floatValue];
            constraintHeight = [NSLayoutConstraint constraintWithItem:subView
                                                            attribute:NSLayoutAttributeHeight
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:self
                                                            attribute:NSLayoutAttributeHeight
                                                           multiplier:0
                                                             constant:heightValue ? heightValue : subView.frame.size.height];
        }
    }
    
    [self addConstraint:constraintHor];
    [self addConstraint:constraintVer];
    constraintWidth ? [self addConstraint:constraintWidth] : nil;
    constraintHeight ? [self addConstraint:constraintHeight] : nil;
}
@end
