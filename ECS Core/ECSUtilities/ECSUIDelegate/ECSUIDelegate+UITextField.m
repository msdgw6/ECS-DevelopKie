//
//  ECUIDelegate+UITextField.m
//  ECDevelopKit
//
//  Created by LittoCats on 8/13/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSUIDelegate+UITextField.h"

@implementation ECSUIDelegate (UITextField)

// return NO to disallow editing.
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;

// became first responder
//- (void)textFieldDidBeginEditing:(UITextField *)textField;

// return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField;

// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
//- (void)textFieldDidEndEditing:(UITextField *)textField;


// return NO to not change text
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

// called when clear button pressed. return NO to ignore (no notifications)
//- (BOOL)textFieldShouldClear:(UITextField *)textField;

// called when 'return' key pressed. return NO to ignore.
//- (BOOL)textFieldShouldReturn:(UITextField *)textField;


@end
