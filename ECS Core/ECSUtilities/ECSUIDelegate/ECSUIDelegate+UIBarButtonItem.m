//
//  ECSUIDelegate+UIBarButtonItem.m
//  ECS DevelopKit
//
//  Created by LittoCats on 9/7/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSUIDelegate+UIBarButtonItem.h"
#import "ECSObjectAttributeWrapper.h"
#import "ECSScriptContext.h"

@implementation ECSUIDelegate (UIBarButtonItem)

- (void)backButtonItemClicked:(UIBarButtonItem *)item
{
    UIViewController *hostVC = ECS_WrappedObjectAttribute(item, ECSUINavigationBarBackButtonItemHostKey);
    [hostVC.navigationController popViewControllerAnimated:YES];
}

- (void)barItemClicked:(UIBarButtonItem *)item
{
    id attributes = ECS_WrappedObjectAttribute(item, ECSUINavigationBarItemAttributesKey);
    if ([attributes[@"action"] conformsToProtocol:@protocol(ECSScriptFunction)])
        [(id<ECSScriptFunction>)(attributes[@"action"]) evaluateWithArguments:nil];
}

@end
