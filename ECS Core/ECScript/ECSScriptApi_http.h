//
//  ECSScriptApi_http.h
//  ECS DevelopKit
//
//  Created by LittoCats on 8/26/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSScriptApi.h"

@interface ECSScriptApi_http : ECSScriptApi


/**
 *  @download 下载文件
 *  @arguments[0] url 不能为空
 *  @arguments[1] parameter 可以为空
 *  @arguments[2] progress function, 下载进度，可以为空
 */
+ (void)ECS_Script_API(download);
@end
