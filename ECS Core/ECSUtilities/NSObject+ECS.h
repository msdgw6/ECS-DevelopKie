//
//  NSObject+EC.h
//  ECDevelopKit
//
//  Created by LittoCats on 8/6/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (EC)

/**
 *  @description NSObject 能像的唯一标识符
 */

@property (nonatomic, readonly) NSString *_id;

/**
 *  @description 根据唯一标识符，找到对像
 */
+ (id)findObjectById:(NSString *)_id;

/**
 *  判断对像是否为空，子类需进一步判断
 *  等于 NSNull 时，返回 YES
 */
- (BOOL)isEmpty;
@end


@interface NSString (EC)

/**
 *  @description
 *  MD5
 *  子类每次调用时，都将重新计算
 */
@property (nonatomic, readonly) NSString *MD5;

/**
 *  @description Base64
 */
@property (nonatomic, readonly) NSString *base64;

/**
 *  @description Base64Decode
 */
@property (nonatomic, readonly) NSData *base64DecodeData;
@property (nonatomic, readonly) NSString *base64DecodeString;

@end

@interface NSDictionary (EC)

@end

@interface NSArray (EC)

@end

@interface NSData (EC)

/**
 *  @description MD5
 *  子类每次调用时，都将重新计算
 */
@property (nonatomic, readonly) NSString *MD5;

/**
 *  @description Base64
 */
@property (nonatomic, readonly) NSString *base64;
@end