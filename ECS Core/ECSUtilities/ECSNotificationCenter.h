//
//  ECNotificationCenter.h
//  ECDevelopKit
//
//  Created by LittoCats on 8/14/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//
//  NSNotificationCenter 的替代，自动回收无效的observer,其它用法
//  与NSNotification相同
//  ios 6.0 以上可用

//NS_AVAILABLE(10_9, 5_0)

#import <Foundation/Foundation.h>

@interface ECSNotification : NSObject

- (NSString *)name;
- (id)object;
- (NSDictionary *)userInfo;

@end

@interface ECSNotificationCenter : NSObject

+ (instancetype)defaultCenter;

- (instancetype)init;	/* designated initializer */

- (void)addObserver:(id)observer selector:(SEL)aSelector name:(NSString *)aName object:(id)anObject;

- (void)postNotification:(NSNotification *)notification;
- (void)postNotificationName:(NSString *)aName object:(id)anObject;
- (void)postNotificationName:(NSString *)aName object:(id)anObject userInfo:(NSDictionary *)aUserInfo;

- (void)removeObserver:(id)observer;
- (void)removeObserver:(id)observer name:(NSString *)aName object:(id)anObject;

- (void)addObserver:(id)observer
               name:(NSString *)aName
             object:(id)obj
              queue:(NSOperationQueue *)queue
         usingBlock:(void (^)(id observer, ECSNotification *noti))block ;

@end

