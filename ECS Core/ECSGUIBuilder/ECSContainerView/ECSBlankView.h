//
//  ECSBlankView.h
//  ECS DevelopKit
//
//  Created by LittoCats on 9/1/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//
//  BlankView 空的view 默认背景 clear,可布局其它的view 也可以用来绘图
//  布局：padding subview及绘图的边界，subview 的宽/高为定值/wrap_content时，可能无效
//      subview.margin .size .align 确定subview的位置，margin 默认为 {left:0,right:0,top:0,bottom:0}, size 默认为{width:0,height:0}，align 默认为 left|top
//
//  BlankView 未实现 wrap_content 功能


#import <UIKit/UIKit.h>

@interface ECSBlankView : UIView


@end
