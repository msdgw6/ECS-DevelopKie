//
//  ECSButton.h
//  ECS DevelopKit
//
//  Created by LittoCats on 9/7/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//
//  ECSButton state : normal or highlighted(pressed)
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(char, ECSButtonStyle) {
    ECSButtonStyleLeftImage         = 'l',   //默认样式
    ECSButtonStyleRightImage        = 'r',
    ECSButtonStyleTopImage          = 't',
    ECSButtonStyleBottomImage       = 'b'
};

@interface ECSButton : UIControl

@property (nonatomic) ECSButtonStyle style;

+ (id)buttonWithStyle:(ECSButtonStyle)style;

@property(nonatomic)          BOOL         adjustsImageWhenHighlighted;    // default is YES. if YES, image is drawn darker when highlighted(pressed)
@property(nonatomic)          BOOL         adjustsImageWhenDisabled;       // default is YES. if YES, image is drawn lighter when disabled

- (void)setTitle:(NSString *)title forState:(UIControlState)state;                     // default is nil. title is assumed to be single line
- (void)setTitleColor:(UIColor *)color forState:(UIControlState)state; // default if nil. use opaque white
- (void)setImage:(UIImage *)image forState:(UIControlState)state;                      // default is nil. should be same size if different for different states
- (void)setBackgroundImage:(UIImage *)image forState:(UIControlState)state; // default is nil

- (NSString *)titleForState:(UIControlState)state;          // these getters only take a single state value
- (UIColor *)titleColorForState:(UIControlState)state;
- (UIImage *)imageForState:(UIControlState)state;
- (UIImage *)backgroundImageForState:(UIControlState)state;

//@property(nonatomic,readonly) NSString *currentTitle;             // normal/highlighted/disabled. can return nil
//@property(nonatomic,readonly) UIColor  *currentTitleColor;        // normal/highlighted/disabled. always returns non-nil. default is white(1,1)
//@property(nonatomic,readonly) UIImage  *currentImage;             // normal/highlighted/disabled. can return nil
//@property(nonatomic,readonly) UIImage  *currentBackgroundImage;   // normal/highlighted/disabled. can return nil

@property(nonatomic,readonly) UILabel     *titleLabel;
@property(nonatomic,readonly) UIImageView *imageView;

@property (nonatomic, readonly) UIControlState state; //normal or highlighted(pressed)

@end
