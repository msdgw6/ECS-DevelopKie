//
//  ECScriptContext.h
//  ECDevelopKit
//
//  Created by LittoCats on 8/6/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ECSScriptContextKey @"context"

@protocol ECSScriptFunction;
@interface ECSScriptContext : NSObject

/**
 *  新建一个 script 执行环境
 */
+ (instancetype)context;

/**
 *  执行 script
 *  @params script A string to evaluate .
 *  return result by evaluate script , type is NSString/NSMutableDictionary/NSMutableArray/NSnumber/NSNull/nil
 */
- (id)evaluateScript:(NSString *)script;

/**
 *  同步调用 script context 中的方法
 *  @prams 类型只能是 NSString/NSMutableDictionary/NSMutableArray/NSnumber/NSNull/nil
 *  return result by evaluate script , type is NSString/NSMutableDictionary/NSMutableArray/NSnumber/NSNull/nil
 */
- (id)callFunction:(NSString *)function withArguments:(id)argument, ... NS_REQUIRES_NIL_TERMINATION;
+ (id)callFunction:(id<ECSScriptFunction>)function withArguments:(id)argument, ... NS_REQUIRES_NIL_TERMINATION;
/**
 *  @ callMethod:ofObject:withArguments:buffer:
 *  @method 已注册的 ScriptApi 接口方法名，可通过 NSSelectorFromString(_method) 获取。
 *  @object 为实例 _id(可通过 `[NSObject findObjectById:_id]` 获取对像实例，ObjectId 必须以小写字母开头)
 *          或为类名称（具有前缀 C_ ，可通过 `NSClassFromString(ClassName)` 获取类）。
 *  @arguments  参数数组
 *  @returnType 返回结果类型
 *
 *  @return value 为 void 指针，可通过
 */
//- (id)callMethod:(NSString *)method ofOwner:(NSString *)owner withArguments:(NSArray *)arguments;
@end

@protocol ECSScriptFunction <NSObject>

/**
 *  执行 function ,并反回结果
 *  @prams 类型只能是 NSString/NSMutableDictionary/NSMutableArray/NSnumber/NSNull/nil
 *  return result by evaluate script , type is NSString/NSMutableDictionary/NSMutableArray/NSnumber/NSNull/nil
 */
- (id)evaluateWithArguments:(id)argument, ... NS_REQUIRES_NIL_TERMINATION;

- (id)evaluateWithArguments:(id)argument va_list:(va_list)vList;

@end