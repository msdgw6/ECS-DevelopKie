//
//  ECSZipArchive.h
//  ECS DevelopKit
//
//  Created by LittoCats on 8/27/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ECSArchiver : NSObject

/**
 *  @unCompress 
 */
+ (void)uncompressFile:(NSString *)file
                    to:(NSString *)path
     completionHanlder:(void (^)(NSError *error))completionHanlder;

@end
