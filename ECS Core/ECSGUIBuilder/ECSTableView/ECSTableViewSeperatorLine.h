//
//  ECSTableViewSeperatorLine.h
//  ECS DevelopKit
//
//  Created by LittoCats on 9/3/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ECSTableViewSeperatorLineOffset(left,right) UIEdgeInsetsMake(0, left, 0, right)
@interface ECSTableViewSeperatorLine : UIView

/**
 *  只有 left right 起作用
 *  默认值 {left:10,right:0}
 */
@property (nonatomic, assign) UIEdgeInsets offset;

/**
 *  分割线高度，默认高度为 1 个像素
 */
@property (nonatomic, assign) CGFloat height;

/**
 *  背景色
 *  若background 为图片，图片将以repeat形式设为背景
 *  默认值 lightGray
 */
@property (nonatomic, strong) id background;
@end
