// Generated by CoffeeScript 1.7.1
(function() {
  var api_list, api_name;

  api_name = "$A";

  api_list = {
    app: ["alert"],
    page: ["open", "close", "param", "didLoad", "willAppear", "didAppear", "willDisappear", "didDisappear", "navigator", "hideNavigationBar"],
    view: ["attr", "refresh", "onClick", "onItemClick"],
    http: ["get", "post", "download", "upload", "listen", "close"],
    fs: ["compress", "unCompress", "loadFile"],
    db: ["cache", "save", "find"]
  };

  ecs_registe_asynchronous_api(api_list, api_name);

}).call(this);
