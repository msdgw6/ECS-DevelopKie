//
//  main.m
//  ECS DevelopKit
//
//  Created by LittoCats on 8/17/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ECSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ECSAppDelegate class]));
    }
}
