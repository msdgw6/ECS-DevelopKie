//
//  ECSScriptApi_app.h
//  ECS DevelopKit
//
//  Created by LittoCats on 8/26/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSScriptApi.h"

@interface ECSScriptApi_app : ECSScriptApi

/**
 *  @alert
 *  @arguments message, buttonTitle, ..., buttonOnclick
 *  @discussion buttonOnClick 为 alertView 按钮的 onClick 事件回调方法，参数为 button 的 position 及 title
 */
+ (void)ECS_Script_API(alert);

@end
