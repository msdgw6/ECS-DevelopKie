//
//  ECSScriptApi_view.h
//  ECS DevelopKit
//
//  Created by LittoCats on 8/26/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSScriptApi.h"

@interface ECSScriptApi_view : ECSScriptApi

/**
 *  属性及数据
 */
+ (void)ECS_Script_API(attr);//设置或获取属性 arguments : [@"NSString",@{}],可以在设置时同时获取
+ (void)ECS_Script_API(refresh);

/**
 *  用户事件
 */
+ (void)ECS_Script_API(onClick);
+ (void)ECS_Script_API(onItemClick);
@end

@protocol ECSScriptApi_view <NSObject>

@optional
- (void)refresh:(id)data;

- (void)onClick:(id)callback;
- (void)onItemClick:(id)callback;

@end