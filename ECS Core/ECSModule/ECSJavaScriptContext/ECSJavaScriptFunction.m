//
//  ECSJavaScriptFunction.m
//  ECS DevelopKit
//
//  Created by LittoCats on 8/29/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSJavaScriptFunction.h"
#import "ECSJavaScriptContext.h"

#import <JavaScriptCore/JavaScriptCore.h>

//@protocol ECSJavaScriptContext <NSObject>
//
//+ (id (^)(JSContextRef ctx, JSObjectRef func, id arg, va_list vList, JSValueRef *exception))invokeJSFunction;
//
//+ (void (^)(JSContextRef, JSValueRef))ExceptionHandler;
//
//@end

@implementation ECSJavaScriptFunction

//- (instancetype)initWithJSContextRef:(JSContextRef)ctx JSObjectRef:(JSObjectRef)jsObj
//{
//    self = [super init];
//    if (self) {
//        self.context = ctx;
//        self.function = jsObj;
//        
//        
//        //protect
//        JSValueProtect(ctx, jsObj);
//    }
//    return self;
//}
//
//- (void)dealloc
//{
//    JSValueUnprotect(_context, _function);
//}
//
//- (BOOL)isValide
//{
//    return NO;
//}
//
///**************************************************************************************************/
//
- (id)applyWithArguments:(id)arg,...
{
    //    if (![self isValide])
    //        return nil;
    
//    va_list vList;
//    va_start(vList, arg);
//    
//    return [self applyWithArguments:arg va_list:vList];
    return nil;
}

- (id)applyWithArguments:(id)arg va_list:(va_list)vList
{
//    JSValueRef exception = NULL;
//    JSContextRef ctx = _context;
//    JSObjectRef func = _function;
//    
//    id result = [(id<ECSJavaScriptContext>)[ECSJavaScriptContext class] invokeJSFunction](ctx,
//                                                                                          func,
//                                                                                          arg,
//                                                                                          vList,
//                                                                                          &exception);
//    [(id<ECSJavaScriptContext>)[ECSJavaScriptContext class] ExceptionHandler](ctx, exception);
//    
//    return result;
    return nil;
}
@end
