//
//  ECSObjectAttributeWrapper.h
//  ECS DevelopKit
//
//  Created by LittoCats on 9/1/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ECSContextPathKey "ECSContextPathKey"
#define ECSUIBuilderContextKey "ECSUIBuilderContextKey"

@interface ECSObjectAttributeWrapper : NSObject

@end

void ECS_WrapObjectAttribute(id object, id attribute, const char *key);

id ECS_WrappedObjectAttribute(id object, const char *key);

/**
 *  弱引用一个 attribute ,object 最多只能拥有一个 attribute.class 类型的属性
 */
void ECS_BoxAttribute(id object, id attribute);

/**
 *  取出 object 中type类型的属性
 */
id ECS_BoxedAttribute(id object, Class type);

id _ECS_WrapValue(const char *type, ...);

#define ECS_WrapValue(value) _ECS_WrapValue(@encode(__typeof__((value))), (value))