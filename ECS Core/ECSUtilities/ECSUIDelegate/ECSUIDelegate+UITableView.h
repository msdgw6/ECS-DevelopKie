//
//  ECSUIDelegate+UITableView.h
//  ECS DevelopKit
//
//  Created by LittoCats on 9/3/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSUIDelegate.h"

#define ECSUITableViewDataSourceKey "ECSUITableViewDataSourceKey"

@interface ECSUIDelegate (UITableView) <UITableViewDataSource, UITableViewDelegate>

@end

@class ECSTableViewSeperatorLine;
@protocol ECSTableSubview <NSObject>

@required
@property (nonatomic, readwrite) NSDictionary *contentData;
/**
 *  @return 根据内容，计算出cell的高度
 *  tableview 在reloadData时根据需要自动调用
 */
+ (CGFloat)heightForContentData:(id)data;

/**
 *  @更新数据，
 *  tableview 在reloadData时根据需要自动调用
 */
- (void)updateContentData;

/**
 *  @
 */
- (NSArray *)contentViews;

@optional
/**
 *  @return contentViews 的自动布局信息
 */
- (NSArray *)contentConstraints;

/**
 *  分割线，cell 实现该方法，显示时，将自动添加分割线
 */
- (ECSTableViewSeperatorLine *)seperatorLine;
@end