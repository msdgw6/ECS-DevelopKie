//
//  ECSDevelopKitTests.m
//  ECSDevelopKitTests
//
//  Created by LittoCats on 8/27/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ECSArchiver.h"

@interface ECSDevelopKitTests : XCTestCase

@end

@implementation ECSDevelopKitTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
//    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
    [ECSArchiver uncompressFile:@"./tmp/ECS DevelopKit.zip" to:@"./Library" completionHanlder:^(NSError *error) {
        if (error) {
            XCTFail(@"%@",error);
        }
    }];
}

@end
