//
//  ECUIDelegate+UITextField.h
//  ECDevelopKit
//
//  Created by LittoCats on 8/13/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSUIDelegate.h"

@interface ECSUIDelegate (UITextField)<UITextFieldDelegate>

@end
