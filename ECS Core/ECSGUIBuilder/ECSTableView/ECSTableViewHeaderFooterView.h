//
//  ECSTableViewHeaderFooterView.h
//  ECS DevelopKit
//
//  Created by LittoCats on 9/3/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ECSTableViewHeaderFooterView : UITableViewHeaderFooterView

@end
