//
//  ECSScriptApi_fs.h
//  ECS DevelopKit
//
//  Created by LittoCats on 8/26/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//
//  根目录为 NSHomeDirectory()
//  可操作的目录为 ./Library ./Documents ./tmp

#import "ECSScriptApi.h"

@interface ECSScriptApi_fs : ECSScriptApi

/**
 *  @loadFile 读取本地文件
 *  @arguments[0] filePath
 
    @discussion 可作为同步方法
 */
+ (NSString *)ECS_Script_API(loadFile);

/**
 *  @unCompress 解压缩文件到指定目录
 *  @arguments[0] filePath 源文件目录
 *  @arguments[1] destination path, 如果为空，则解压到源文件所在的目录
 */
+ (void)ECS_Script_API(unCompress);
@end
