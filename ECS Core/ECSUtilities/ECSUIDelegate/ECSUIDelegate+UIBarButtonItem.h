//
//  ECSUIDelegate+UIBarButtonItem.h
//  ECS DevelopKit
//
//  Created by LittoCats on 9/7/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSUIDelegate.h"

#define ECSUINavigationBarBackButtonItemHostKey "ECSUINavigationBarBackButtonItemHostKey"

#define ECSUINavigationBarItemAttributesKey "ECSUINavigationBarItemAttributesKey"

@interface ECSUIDelegate (UIBarButtonItem)

- (void)backButtonItemClicked:(UIBarButtonItem *)item;

- (void)leftItemClicked:(UIBarButtonItem *)item;

- (void)rightItemClicked:(UIBarButtonItem *)item;

@end
