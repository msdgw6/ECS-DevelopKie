//
//  ECSTableViewCell.m
//  ECS DevelopKit
//
//  Created by LittoCats on 9/3/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import "ECSTableViewCell.h"
#import "ECSTableView.h"
#import "ECSTableViewSeperatorLine.h"

#import "ECSUIDelegate+UITableView.h"

@interface ECSTableViewCell ()

@property (nonatomic, strong) NSDictionary *contentData;

@end

@implementation ECSTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        NSAssert([self conformsToProtocol:@protocol(ECSTableSubview)], @"[%@] error , ECSTableViewCell 的子类必须实现 ECSTableSubview 协议。",self.class);
        NSArray *contents = [(id<ECSTableSubview>)self contentViews];
        for (UIView *subview in contents)
            [self.contentView addSubview:subview];
        if ([self respondsToSelector:@selector(contentConstraints)])
            for (NSLayoutConstraint *constraint in [(id<ECSTableSubview>)self contentConstraints]) {
                ((UIView *)constraint.firstItem).translatesAutoresizingMaskIntoConstraints = NO;
                [self.contentView addConstraint:constraint];
            }
        if ([self respondsToSelector:@selector(seperatorLine)])
            [self.contentView addSubview:[(id<ECSTableSubview>)self seperatorLine]];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
//    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark-
- (void)setContentData:(NSDictionary *)contentData
{
    _contentData = contentData;
    [(id<ECSTableSubview>)self updateContentData];
}
@end
