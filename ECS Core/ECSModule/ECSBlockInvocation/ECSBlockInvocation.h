//
//  ECSBlockInvocation.h
//  ECS DevelopKit
//
//  Created by LittoCats on 8/29/14.
//  Copyright (c) 2014 Littocats. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ECSBlockInvocation : NSObject

+ (id)invokeBlock:(id)block withArguments:(NSArray *)arguments;

+ (id)invokeBlock:(id)block withArguments:(NSArray *)arguments returnType:(const char **)returnType;
@end
